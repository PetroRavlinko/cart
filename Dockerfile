FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/*.jar app.jar

ENV JAVA_OPTS ""

ENV SPRING_BOOT_PARAMETERS ""
ENV SERVER_PORT 8080

EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom $SPRING_BOOT_PARAMETERS -jar /app.jar" ]
