# Cart [![pipeline status](https://gitlab.com/PetroRavlinko/cart/badges/master/pipeline.svg)](https://gitlab.com/PetroRavlinko/cart/commits/master) [![coverage report](https://gitlab.com/PetroRavlinko/cart/badges/master/coverage.svg)](https://gitlab.com/PetroRavlinko/cart/commits/master)

## Getting Started
### Prerequisites
### Installing

## Running the tests
### Break down into end to end tests
```jshelllanguage
mvn verify 
```
### Check unit test quality
```jshelllanguage
mvn clean install org.pitest:pitest-maven:mutationCoverage
```

### And coding style tests
## Deployment
## Built With
[Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/PetroRavlinko/cart/tags). 

## Authors

* **Petro Ravlinko** - *Initial work* - [PurpleBooth](https://github.com/PetroRavlinko)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

[README TEMPLATE](https://gist.github.com/PurpleBooth/b24679402957c63ec426)