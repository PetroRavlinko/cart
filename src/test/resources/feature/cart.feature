Feature: Shopping cart

  Background:
    * url baseUrl

  Scenario: get a cart
    Given path 1, '/'
    When method GET
    Then status 200
    And match $ == {id: 1, totalPrice: 0.0, items: []}

  Scenario: add product to cart
    Given path 1, 'items'
    And request {qty: 2, id: 42}
    When method POST
    Then status 201
    And match $ == {id: 1, qty: 2, price: 12.34, product: {id: 42}}

    Given path 1, '/'
    When method GET
    Then status 200
    And match $ == {id: 1, totalPrice: 24.68, items: [{id: 1, qty: 2, price: 12.34, product: {id: 42}}]}

  Scenario: add promotion to cart
    Given path 2, 'items'
    And request {qty: 2, id: 42}
    When method POST
    Then status 201
    And match $ == {id: 2, qty: 2, price: 12.34, product: {id: 42}}

    Given path 2, 'promotions'
    And request {id: 42, code: PROMO}
    When method POST
    Then status 201
    And match $ == {id: 42, code: "PROMO"}

    Given path 2, '/'
    When method GET
    Then status 200
    And match $ == {id: 2, totalPrice: 23.45, items: [{id: 2, qty: 2, price: 12.34, product: {id: 42}}], promotion: {id: #(response.promotion.id), code: "PROMO"}}

  Scenario: delete a cart
    Given path 3, '/'
    When method GET
    Then status 200

    Given path 3, '/'
    When method DELETE
    Then status 200
    And match $ == {"id": 3}

  Scenario: get cart items
    Given path 4, '/'
    When method GET
    Then status 200

    Given path 4, 'items'
    When method GET
    Then status 200
    And match $ == []

  Scenario: update cart item
    Given path 5, '/'
    When method GET
    Then status 200

    Given path 5, 'items'
    And request {qty: 2, id: 42}
    When method POST
    Then status 201

    Given path 5, 'items', response.id
    And request {qty: 3, id: #(response.id)}
    When method PUT
    Then status 200
    And match $ == {id: #(response.id), qty: 3, price: 12.34, product: {id: 42}}

    Given path 5, '/'
    When method GET
    Then status 200
    And match $ == {id: 5, totalPrice: 37.02, items: [{id: #notnull, qty: 3, price: 12.34, product: {id: 42}}]}

  Scenario: remove item from cart
    Given path 6, 'items'
    And request {qty: 2, id: 42}
    When method POST
    Then status 201
    And match $ == {id: #notnull, qty: 2, price: 12.34, product: {id: 42}}

    Given path 6, 'items', response.id
    When method DELETE
    Then status 200
    And match $ == {"id": #(response.id)}

    Given path 6, '/'
    When method GET
    Then status 200
    And match $ == {"id": 6, totalPrice: 0.0, "items": []}