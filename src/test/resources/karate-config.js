function properties() {
    var env = karate.env; // get java system property 'karate.env'
    karate.log('karate.env system property was:', env);
    if (!env) {
        env = 'dev'; // a custom 'intelligent' default
    }
    var port = karate.properties['local.server.port'];
    return {
        baseUrl: 'http://localhost:' + port
    }
}