package com.ravlinko.ecommerce.cart.facades;

import com.ravlinko.ecommerce.cart.dtos.*;
import com.ravlinko.ecommerce.cart.mappers.CartMapper;
import com.ravlinko.ecommerce.cart.mappers.ItemMapper;
import com.ravlinko.ecommerce.cart.mappers.PromotionMapper;
import com.ravlinko.ecommerce.cart.models.CartModel;
import com.ravlinko.ecommerce.cart.models.ItemModel;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.models.PromotionModel;
import com.ravlinko.ecommerce.cart.services.DefaultCartService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CartFacadeTest {
    private static final long CART_ID = 1L;
    private static final long CART_ITEM_ID = 42L;

    @Mock
    private DefaultCartService cartService;
    @Mock
    private CartMapper cartMapper;
    @Mock
    private ItemMapper itemMapper;
    @Mock
    private PromotionMapper promotionMapper;
    @InjectMocks
    private DefaultCartFacade cartFacade;

    @Mock
    private CartModel mockCart;
    @Mock
    private ItemModel mockItem;

    @Test
    public void itShouldBeAbleToGetCartByID() {
        CartDto expectedItem = new CartDto();
        when(cartService.getCartByID(eq(CART_ID))).thenReturn(mockCart);
        when(cartMapper.mapToDto(mockCart)).thenReturn(expectedItem);

        CartDto actualCart = cartFacade.getCart(CART_ID);

        verify(cartService, only()).getCartByID(eq(CART_ID));
        verify(cartMapper, only()).mapToDto(eq(mockCart));

        assertEquals(expectedItem, actualCart);
    }

    @Test
    public void itShouldBeAbleToAddItemToCart() {
        ItemDto expectedItem = new ItemDto();
        long productId = CART_ITEM_ID;
        ProductModel product42 = new ProductModel(productId);
        int productQty = 2;
        ItemForm itemForm = new ItemForm();
        itemForm.setId(productId);
        itemForm.setQty(productQty);
        when(cartService.addItem(eq(CART_ID), eq(product42), eq(productQty))).thenReturn(mockItem);
        when(itemMapper.map(mockItem)).thenReturn(expectedItem);

        ItemDto actualItem = cartFacade.addToCart(CART_ID, itemForm);

        verify(cartService, only()).addItem(eq(CART_ID), eq(product42), eq(productQty));
        verify(itemMapper, only()).map(eq(mockItem));

        assertThat(actualItem, is(expectedItem));
    }

    @Test
    public void itShouldBeAbleToAddPromotionToCart() {
        PromotionDto expectedPromo = new PromotionDto();
        long id = 42L;
        String promoCode = "PROMO";
        PromotionModel promo = new PromotionModel(id, promoCode);
        PromotionForm promotionForm = new PromotionForm();
        promotionForm.setId(id);
        promotionForm.setCode(promoCode);

        when(cartService.addPromotion(eq(CART_ID), eq(promo))).thenReturn(new PromotionModel(id, promoCode));
        when(promotionMapper.map(eq(new PromotionModel(id, promoCode)))).thenReturn(expectedPromo);

        PromotionDto actualPromo = cartFacade.addPromotionToCart(CART_ID, promotionForm);

        verify(cartService, only()).addPromotion(eq(CART_ID), eq(promo));
        verify(promotionMapper, only()).map(eq(new PromotionModel(id, promoCode)));

        assertThat(actualPromo, is(expectedPromo));
    }

    @Test
    public void itShouldBeAbleToGetAllCartItems() {
        ItemModel item42 = new ItemModel();
        item42.setId(CART_ITEM_ID);
        ItemModel item43 = new ItemModel();
        item43.setId(43L);
        List<ItemDto> expectedCartItems = new ArrayList<>();

        when(cartService.getCartItems(eq(CART_ID))).thenReturn(Arrays.asList(item42, item43));
        when(itemMapper.map(eq(Arrays.asList(item42, item43)))).thenReturn(expectedCartItems);

        List<ItemDto> actualCartItems = cartFacade.getCartItems(CART_ID);

        verify(cartService, only()).getCartItems(eq(CART_ID));
        verify(itemMapper, only()).map(eq(Arrays.asList(item42, item43)));

        assertThat(actualCartItems, is(expectedCartItems));
    }

    @Test
    public void itShouldBeAbleToUpdateCartItem() {
        ItemDto expectedItem = new ItemDto();
        long itemId = CART_ITEM_ID;
        int newQty = 2;
        ItemForm itemForm = new ItemForm();
        itemForm.setId(itemId);
        itemForm.setQty(newQty);

        when(cartService.updateCartItem(eq(itemId), eq(newQty))).thenReturn(new ItemModel());
        when(itemMapper.map(eq(new ItemModel()))).thenReturn(expectedItem);

        ItemDto actualItem = cartFacade.updateCartItem(itemForm);

        verify(cartService).updateCartItem(eq(itemId), eq(newQty));
        verify(itemMapper, only()).map(eq(new ItemModel()));

        assertThat(actualItem, is(expectedItem));
    }

    @Test
    public void itShouldBeAbleToRemoveCartById() {
        IdDto expectedRemovedCart = new IdDto(CART_ID);
        when(cartService.deleteCart(eq(CART_ID))).thenReturn(CART_ID);

        IdDto actualRemovedCart = cartFacade.deleteCart(CART_ID);

        verify(cartService, only()).deleteCart(eq(CART_ID));

        assertThat(actualRemovedCart, is(expectedRemovedCart));
    }

    @Test
    public void itShouldBeAbleToRemoveItemFromCart() {
        long cartItemId = CART_ITEM_ID;
        IdDto expectedRemovedItem = new IdDto(cartItemId);
        when(cartService.deleteCartItem(eq(CART_ID), eq(cartItemId))).thenReturn(cartItemId);

        IdDto actualRemovedItem = cartFacade.removeCartItem(CART_ID, cartItemId);

        verify(cartService, only()).deleteCartItem(eq(CART_ID), eq(cartItemId));

        assertThat(actualRemovedItem, is(expectedRemovedItem));
    }

}