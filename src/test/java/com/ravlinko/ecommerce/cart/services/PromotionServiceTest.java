package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.dtos.DiscountDto;
import com.ravlinko.ecommerce.cart.models.PromotionModel;
import com.ravlinko.ecommerce.cart.services.adapters.PromotionServiceAdapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PromotionServiceTest {
    private static final Long PROMO_ID = 42L;
    private static final double DISCOUNT_PERCENT = 5.0;
    @Mock
    private PromotionServiceAdapter promotionServiceAdapter;
    @InjectMocks
    private DefaultPromotionService promotionService;

    @Mock
    private Call<DiscountDto> mockPriceCall;

    @Test
    public void itShouldProvidePromotionPercentDiscount() throws IOException {
        Response<DiscountDto> fakeResponse = Response.success(new DiscountDto(DISCOUNT_PERCENT));
        when(mockPriceCall.execute()).thenReturn(fakeResponse);
        when(promotionServiceAdapter.getDiscountByPromotionId(eq(PROMO_ID))).thenReturn(mockPriceCall);

        Double actualDiscount = promotionService.getPromotionDiscount(new PromotionModel(PROMO_ID, "PROMO"));

        assertThat(actualDiscount, is(DISCOUNT_PERCENT));
    }

}