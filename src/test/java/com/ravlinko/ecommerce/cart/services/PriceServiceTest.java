package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.dtos.PriceDto;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.services.adapters.PriceServiceAdapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PriceServiceTest {
    private static final long PRODUCT_ID = 42L;
    @Mock
    private PriceServiceAdapter priceServiceAdapter;
    @InjectMocks
    private DefaultPriceService priceService;

    @Mock
    private Call<PriceDto> mockPriceCall;

    @Test
    public void itShouldProvidePriceForSpecificProduct() throws IOException {
        Response<PriceDto> fakeResponse = Response.success(new PriceDto(12.34));
        when(mockPriceCall.execute()).thenReturn(fakeResponse);
        when(priceServiceAdapter.getPriceByProductId(eq(PRODUCT_ID))).thenReturn(mockPriceCall);

        Double actualPrice = priceService.getPriceFor(new ProductModel(PRODUCT_ID));

        verify(priceServiceAdapter, only()).getPriceByProductId(eq(PRODUCT_ID));
        assertThat(actualPrice, is(12.34));
    }

}