package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.models.CartModel;
import com.ravlinko.ecommerce.cart.models.ItemModel;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.models.PromotionModel;
import com.ravlinko.ecommerce.cart.repositories.CartItemRepository;
import com.ravlinko.ecommerce.cart.repositories.CartRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceTest {

    @Mock
    private CartRepository cartRepository;
    @Mock
    private CartItemRepository cartItemRepository;
    @Mock
    private DefaultPriceService priceService;
    @Mock
    private DefaultPromotionService promotionService;
    @Mock
    private CartModel mockCart;
    @Mock
    private ItemModel mockItem;

    @InjectMocks
    private DefaultCartService cartService;

    private ProductModel product = new ProductModel(42L);
    private ItemModel expectedCartItem;

    @Before
    public void setUp() {
        expectedCartItem = new ItemModel();
        expectedCartItem.setId(0L);
        expectedCartItem.setPrice(12.34);
        expectedCartItem.setProduct(product);
        expectedCartItem.setCart(mockCart);

        when(priceService.getPriceFor(any(ProductModel.class))).thenReturn(12.34);
        when(mockItem.getProduct()).thenReturn(product);
        when(mockItem.getPrice()).thenReturn(12.34);
        when(cartRepository.findById(anyLong())).thenReturn(Optional.of(mockCart));
    }

    @Test
    public void itShouldAddAppropriateQtyOfProductToCart() {
        int qty = 2;
        expectedCartItem.setId(null);
        expectedCartItem.setQty(qty);

        cartService.addItem(1L, product, qty);

        verify(cartItemRepository).save(eq(expectedCartItem));
    }

    @Test
    public void itShouldMargeAddedProductsToCart() {
        when(mockItem.getQty()).thenReturn(1);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(Collections.singletonList(mockItem)));

        cartService.addItem(1L, product, 1);


        verify(mockItem).setQty(2);
        verify(cartItemRepository).save(eq(mockItem));
    }

    @Test
    public void itShouldMargeAddedQtyOfProductsToCart() {
        when(mockItem.getQty()).thenReturn(2);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(Collections.singletonList(mockItem)));

        cartService.addItem(1L, product, 3);

        verify(cartItemRepository).save(eq(mockItem));
    }

    @Test
    public void itShouldUpdateCartItemQty() {
        when(mockItem.getQty()).thenReturn(2);
        when(mockItem.getCart()).thenReturn(mockCart);
        when(cartItemRepository.findById(eq(42L))).thenReturn(Optional.of(mockItem));
        when(mockCart.getItems()).thenReturn(new LinkedList<>(Collections.singletonList(mockItem)));

        cartService.updateCartItem(42L, 3);

        verify(cartItemRepository).save(eq(mockItem));
    }

    @Test
    public void itShouldRetrieveAllCartItems() {
        when(mockCart.getItems()).thenReturn(new LinkedList<>(Collections.singletonList(mockItem)));

        cartService.getCartItems(1L);

        verify(mockCart, only()).getItems();
    }

    @Test
    public void itShouldAddPromotion() {
        PromotionModel promotion = new PromotionModel(43L, "PROMO");

        cartService.addPromotion(1L, promotion);

        verify(mockCart).setPromotion(eq(promotion));
    }

    @Test
    public void itShouldRemoveCartItem() {
        when(mockItem.getId()).thenReturn(42L);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(new ArrayList<>(Collections.singletonList(mockItem))));

        cartService.deleteCartItem(1L, 42L);

        verify(cartItemRepository).delete(eq(mockItem));
    }

    @Test
    public void itShouldDeleteCart() {
        cartService.deleteCart(1L);

        verify(cartRepository).deleteById(eq(1L));
    }

    @Test
    public void itShouldRecalculateCartAfterAddingItem() {
        when(mockItem.getQty()).thenReturn(2);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(Collections.singletonList(mockItem)));

        cartService.addItem(1L, product, 1);

        verify(mockCart).setTotalPrice(eq(24.68));
    }

    @Test
    public void itShouldRecalculateCartAfterUpdatingItem() {
        when(mockItem.getQty()).thenReturn(2);
        when(mockItem.getCart()).thenReturn(mockCart);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(Collections.singletonList(mockItem)));
        when(cartItemRepository.findById(eq(42L))).thenReturn(Optional.of(mockItem));

        cartService.updateCartItem(42L, 2);

        verify(mockCart).setTotalPrice(eq(24.68));
    }

    @Test
    public void itShouldRecalculateCartAfterDeleteItem() {
        when(mockItem.getId()).thenReturn(42L);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(new ArrayList<>(Collections.singletonList(mockItem))));

        cartService.deleteCartItem(1L, 42L);

        verify(mockCart).setTotalPrice(eq(.0));
    }

    @Test
    public void itShouldRecalculateCartAfterAddPromotion() {
        PromotionModel promotion = new PromotionModel(42L, "PROMO");
        when(mockItem.getQty()).thenReturn(2);
        when(mockCart.getPromotion()).thenReturn(promotion);
        when(mockCart.getItems()).thenReturn(new LinkedList<>(new ArrayList<>(Collections.singletonList(mockItem))));
        when(promotionService.getPromotionDiscount(eq(promotion))).thenReturn(5.0);

        cartService.addPromotion(1L, promotion);

        verify(promotionService).getPromotionDiscount(eq(promotion));
        verify(mockCart).setTotalPrice(eq(23.45));
    }

}