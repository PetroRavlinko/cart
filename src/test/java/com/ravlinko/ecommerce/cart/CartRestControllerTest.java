package com.ravlinko.ecommerce.cart;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ravlinko.ecommerce.cart.dtos.*;
import com.ravlinko.ecommerce.cart.facades.DefaultCartFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CartRestController.class)
public class CartRestControllerTest {
    private static final Long CART_ID = 1L;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DefaultCartFacade cartFacade;

    @Test
    public void itShouldGetCartByID() throws Exception {
        CartDto cart = new CartDto();
        cart.setId(CART_ID);
        cart.setTotalPrice(.0);
        cart.setItems(Collections.emptyList());

        when(cartFacade.getCart(eq(1L))).thenReturn(cart);

        mockMvc.perform(get("/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"totalPrice\":0.0,\"items\":[]}"));

        verify(cartFacade, only()).getCart(eq(CART_ID));
    }

    @Test
    public void itShouldBeAbleToAddItemToCart() throws Exception {
        ItemForm itemForm = new ItemForm();
        ItemDto itemDto = new ItemDto();
        itemDto.setId(2L);
        itemDto.setPrice(12.34);
        itemDto.setQty(2);
        itemDto.setProduct(new ProductDto(42L));
        when(cartFacade.addToCart(eq(CART_ID), eq(itemForm))).thenReturn(itemDto);

        mockMvc.perform(post("/1/items")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(itemForm)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().json("{\"id\":2,\"qty\":2,\"price\":12.34,\"product\":{\"id\":42}}"));

        verify(cartFacade, only()).addToCart(eq(CART_ID), eq(itemForm));
    }

    @Test
    public void itShouldBeAbleToAddPromotionToCart() throws Exception {
        PromotionForm promotionForm = new PromotionForm();
        promotionForm.setId(42L);
        promotionForm.setCode("PROMO");
        PromotionDto promotionDto = new PromotionDto();
        promotionDto.setId(42L);
        promotionDto.setCode("PROMO");
        when(cartFacade.addPromotionToCart(eq(CART_ID), eq(promotionForm))).thenReturn(promotionDto);

        mockMvc.perform(post("/1/promotions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(promotionForm)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().json("{\"id\":42,\"code\":\"PROMO\"}"));

        verify(cartFacade, only()).addPromotionToCart(eq(CART_ID), eq(promotionForm));
    }

    @Test
    public void itShouldBeAbleToGetAllCartItems() throws Exception {
        ItemDto item42 = new ItemDto();
        item42.setId(1L);
        item42.setQty(1);
        item42.setPrice(12.34);
        item42.setProduct(new ProductDto(42L));
        ItemDto item43 = new ItemDto();
        item43.setId(2L);
        item43.setQty(2);
        item43.setPrice(12.43);
        item43.setProduct(new ProductDto(43L));
        when(cartFacade.getCartItems(eq(CART_ID))).thenReturn(Arrays.asList(item42, item43));

        mockMvc.perform(get("/1/items"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":1,\"qty\":1,\"price\":12.34,\"product\":{\"id\":42}},{\"id\":2,\"qty\":2,\"price\":12.43,\"product\":{\"id\":43}}]"));

        verify(cartFacade, only()).getCartItems(eq(CART_ID));
    }

    @Test
    public void itShouldBeAbleToUpdateCartItem() throws Exception {
        ItemForm itemForm = new ItemForm();
        itemForm.setId(2L);
        itemForm.setQty(2);
        ItemDto itemDto = new ItemDto();
        itemDto.setId(2L);
        itemDto.setPrice(12.34);
        itemDto.setQty(2);
        itemDto.setProduct(new ProductDto(42L));
        when(cartFacade.updateCartItem(eq(itemForm))).thenReturn(itemDto);

        mockMvc.perform(put("/1/items/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(itemForm)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":2,\"qty\":2,\"price\":12.34,\"product\":{\"id\":42}}"));

        verify(cartFacade, only()).updateCartItem(eq(itemForm));

    }

    @Test
    public void itShouldBeAbleToRemoveCartById() throws Exception {
        when(cartFacade.deleteCart(eq(CART_ID))).thenReturn(new IdDto(CART_ID));

        mockMvc.perform(delete("/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1}"));

        verify(cartFacade, only()).deleteCart(eq(CART_ID));
    }

    @Test
    public void itShouldBeAbleToRemoveItemFromCart() throws Exception {
        long itemId = 1L;
        when(cartFacade.removeCartItem(eq(CART_ID),eq(itemId))).thenReturn(new IdDto(itemId));

        mockMvc.perform(delete("/1/items/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1}"));

        verify(cartFacade, only()).removeCartItem(eq(CART_ID),eq(itemId));
    }
}