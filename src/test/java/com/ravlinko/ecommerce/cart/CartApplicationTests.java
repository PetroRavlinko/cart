package com.ravlinko.ecommerce.cart;

import com.ravlinko.ecommerce.cart.models.CartModel;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.services.DefaultCartService;
import com.ravlinko.ecommerce.cart.services.DefaultPriceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CartApplicationTests {
    @Autowired
    private DefaultCartService cartService;

    @MockBean
    private DefaultPriceService priceService;

    @Before
    public void setUp() {
        when(priceService.getPriceFor(any(ProductModel.class))).thenReturn(12.34);
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void itShouldReturnCurrentCart() {
        CartModel cart = cartService.getCartByID(1L);

        assertNotNull(cart);
    }

    @Test
    public void itShouldAddProductToCart() {
        ProductModel product = new ProductModel(42L);
        CartModel cart = cartService.getCartByID(1L);

        cartService.addItem(cart.getId(), product, 1);

        assertThat(cartService.getCartByID(1L).getItems(), contains(
                hasProperty("product", equalTo(product))
        ));
    }

}
