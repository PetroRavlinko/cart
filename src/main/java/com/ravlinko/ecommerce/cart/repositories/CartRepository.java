package com.ravlinko.ecommerce.cart.repositories;

import com.ravlinko.ecommerce.cart.models.CartModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<CartModel, Long> {
}
