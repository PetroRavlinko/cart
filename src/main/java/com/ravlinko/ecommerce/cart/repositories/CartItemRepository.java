package com.ravlinko.ecommerce.cart.repositories;

import com.ravlinko.ecommerce.cart.models.ItemModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemRepository extends CrudRepository<ItemModel, Long> {
}
