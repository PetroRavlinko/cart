package com.ravlinko.ecommerce.cart;

import com.ravlinko.ecommerce.cart.services.adapters.PriceServiceAdapter;
import com.ravlinko.ecommerce.cart.services.adapters.PromotionServiceAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SpringBootApplication
public class CartApplication {
    @Bean
    public PriceServiceAdapter priceServiceAdapter() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:8089")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
         return retrofit.create(PriceServiceAdapter.class);
    }

    @Bean
    public PromotionServiceAdapter promotionServiceAdapter() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:8089")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
         return retrofit.create(PromotionServiceAdapter.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CartApplication.class, args);
    }
}
