package com.ravlinko.ecommerce.cart;

import com.ravlinko.ecommerce.cart.dtos.ItemForm;
import com.ravlinko.ecommerce.cart.dtos.PromotionForm;
import com.ravlinko.ecommerce.cart.facades.CartFacade;
import com.ravlinko.ecommerce.cart.facades.DefaultCartFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartRestController {

    private final CartFacade cartFacade;

    @Autowired
    public CartRestController(DefaultCartFacade cartFacade) {
        this.cartFacade = cartFacade;
    }

    @RequestMapping(path = "/{cartId}", method = RequestMethod.GET)
    public ResponseEntity getCartById(@PathVariable Long cartId) {
        return ResponseEntity.ok(cartFacade.getCart(cartId));
    }

    @RequestMapping(path = "/{cartId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCart(@PathVariable Long cartId) {
        return ResponseEntity.ok(cartFacade.deleteCart(cartId));
    }

    @RequestMapping(path = "/{cartId}/promotions", method = RequestMethod.POST)
    public ResponseEntity addPromotionToCart(@PathVariable long cartId, @RequestBody PromotionForm cartPromotionForm) {
        return ResponseEntity.status(201).body(cartFacade.addPromotionToCart(cartId, cartPromotionForm));
    }

    @RequestMapping(path = "/{cartId}/items", method = RequestMethod.POST)
    public ResponseEntity addProductToCart(@PathVariable long cartId, @RequestBody ItemForm cartItemForm) {
        return ResponseEntity.status(201).body(cartFacade.addToCart(cartId, cartItemForm));
    }

    @RequestMapping(path = "/{cartId}/items", method = RequestMethod.GET)
    public ResponseEntity getCartItems(@PathVariable long cartId) {
        return ResponseEntity.ok(cartFacade.getCartItems(cartId));
    }

    @RequestMapping(path = "/{cartId}/items/{itemId}", method = RequestMethod.PUT)
    public ResponseEntity updateCartItem(@PathVariable long cartId, @PathVariable long itemId, @RequestBody ItemForm item) {
        assert itemId == item.getId();
        return ResponseEntity.ok(cartFacade.updateCartItem(item));
    }


    @RequestMapping(path = "/{cartId}/items/{itemId}", method = RequestMethod.DELETE)
    public ResponseEntity removeCartItem(@PathVariable long cartId, @PathVariable long itemId) {
        return ResponseEntity.ok(cartFacade.removeCartItem(cartId, itemId));
    }

}
