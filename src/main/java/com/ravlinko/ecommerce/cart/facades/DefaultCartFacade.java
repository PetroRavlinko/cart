package com.ravlinko.ecommerce.cart.facades;

import com.ravlinko.ecommerce.cart.dtos.*;
import com.ravlinko.ecommerce.cart.mappers.CartMapper;
import com.ravlinko.ecommerce.cart.mappers.ItemMapper;
import com.ravlinko.ecommerce.cart.mappers.PromotionMapper;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.models.PromotionModel;
import com.ravlinko.ecommerce.cart.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultCartFacade implements CartFacade {
    private final CartService cartService;
    private final CartMapper cartMapper;
    private final ItemMapper itemMapper;
    private final PromotionMapper promotionMapper;

    @Autowired
    public DefaultCartFacade(CartService cartService, CartMapper cartMapper, ItemMapper itemMapper, PromotionMapper promotionMapper) {
        this.cartService = cartService;
        this.cartMapper = cartMapper;
        this.itemMapper = itemMapper;
        this.promotionMapper = promotionMapper;
    }

    @Override
    public CartDto getCart(Long cartId) {
        return cartMapper.mapToDto(cartService.getCartByID(cartId));
    }

    @Override
    public PromotionDto addPromotionToCart(long cartId, PromotionForm promotionForm) {
        return promotionMapper.map(cartService.addPromotion(cartId, new PromotionModel(promotionForm.getId(), promotionForm.getCode())));
    }

    @Override
    public ItemDto addToCart(long cartId, ItemForm itemForm) {
        return itemMapper.map(cartService.addItem(cartId, new ProductModel(itemForm.getId()), itemForm.getQty()));
    }

    @Override
    public IdDto deleteCart(long cartId) {
        return new IdDto(cartService.deleteCart(cartId));
    }

    @Override
    public List<ItemDto> getCartItems(long cartId) {
        return itemMapper.map(cartService.getCartItems(cartId));
    }

    @Override
    public ItemDto updateCartItem(ItemForm item) {
        return itemMapper.map(cartService.updateCartItem(item.getId(), item.getQty()));
    }

    @Override
    public IdDto removeCartItem(long cartId, long itemId) {
        return new IdDto(cartService.deleteCartItem(cartId, itemId));
    }
}
