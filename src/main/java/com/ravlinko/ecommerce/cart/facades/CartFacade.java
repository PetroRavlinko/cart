package com.ravlinko.ecommerce.cart.facades;

import com.ravlinko.ecommerce.cart.dtos.*;

import java.util.List;

public interface CartFacade {
    CartDto getCart(Long cartId);

    PromotionDto addPromotionToCart(long cartId, PromotionForm promotionForm);

    ItemDto addToCart(long cartId, ItemForm itemForm);

    IdDto deleteCart(long cartId);

    List<ItemDto> getCartItems(long cartId);

    ItemDto updateCartItem(ItemForm item);

    IdDto removeCartItem(long cartId, long itemId);
}
