package com.ravlinko.ecommerce.cart.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class CartModel {
    @Id
    private Long id;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "id", column = @Column(name = "promotion_id"))
    })
    private PromotionModel promotion;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
    private List<ItemModel> items;

    private Double totalPrice;
}
