package com.ravlinko.ecommerce.cart.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class ItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "id", column = @Column(name = "product_id"))
    })
    private ProductModel product;
    private Double price;
    private int qty;

    @ManyToOne
    @JoinColumn(name = "cart_id")
    private CartModel cart;
}
