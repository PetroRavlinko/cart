package com.ravlinko.ecommerce.cart.mappers;

import com.ravlinko.ecommerce.cart.dtos.ItemDto;
import com.ravlinko.ecommerce.cart.models.ItemModel;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ItemMapper {
    ItemDto map(ItemModel item);

    List<ItemDto> map(List<ItemModel> items);
}
