package com.ravlinko.ecommerce.cart.mappers;

import com.ravlinko.ecommerce.cart.dtos.PromotionDto;
import com.ravlinko.ecommerce.cart.models.PromotionModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PromotionMapper {
    PromotionDto map(PromotionModel promotion);
}
