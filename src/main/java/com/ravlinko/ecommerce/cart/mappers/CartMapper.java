package com.ravlinko.ecommerce.cart.mappers;

import com.ravlinko.ecommerce.cart.dtos.CartDto;
import com.ravlinko.ecommerce.cart.models.CartModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartMapper {
    CartDto mapToDto(CartModel cart);
}
