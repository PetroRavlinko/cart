package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.models.ProductModel;

public interface PriceService {
    Double getPriceFor(ProductModel product);
}
