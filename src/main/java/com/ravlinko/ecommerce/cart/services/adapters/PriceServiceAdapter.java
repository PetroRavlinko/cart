package com.ravlinko.ecommerce.cart.services.adapters;

import com.ravlinko.ecommerce.cart.dtos.PriceDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface PriceServiceAdapter {
    @GET("{productId}/price")
    Call<PriceDto> getPriceByProductId(@Path("productId") Long productId);
}
