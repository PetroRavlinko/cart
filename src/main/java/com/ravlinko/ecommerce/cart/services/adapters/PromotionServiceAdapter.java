package com.ravlinko.ecommerce.cart.services.adapters;

import com.ravlinko.ecommerce.cart.dtos.DiscountDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PromotionServiceAdapter {
    @GET("/discounts/{promotionId}")
    Call<DiscountDto> getDiscountByPromotionId(@Path("promotionId") Long promotionId);
}
