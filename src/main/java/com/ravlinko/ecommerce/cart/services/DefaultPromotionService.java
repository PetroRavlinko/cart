package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.models.PromotionModel;
import com.ravlinko.ecommerce.cart.services.adapters.PromotionServiceAdapter;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DefaultPromotionService implements PromotionService {
    private final PromotionServiceAdapter promotionServiceAdapter;

    public DefaultPromotionService(PromotionServiceAdapter promotionServiceAdapter) {
        this.promotionServiceAdapter = promotionServiceAdapter;
    }

    @Override
    public double getPromotionDiscount(PromotionModel promotion) {
        double discount;
        try {
            discount = getDiscountByPromotion(promotion);
            assert .0 < discount && discount < 100.0;
        } catch (Exception e) {
            // TODO change exception
            throw new RuntimeException();
        }
        return discount;
    }

    private Double getDiscountByPromotion(PromotionModel promotion) throws java.io.IOException {
        return Optional.ofNullable(promotionServiceAdapter.getDiscountByPromotionId(promotion.getId()).execute().body())
                // TODO change exception
                .orElseThrow(RuntimeException::new)
                .getPercent();
    }
}
