package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.models.CartModel;
import com.ravlinko.ecommerce.cart.models.ItemModel;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.models.PromotionModel;

import java.util.List;

public interface CartService {
    CartModel getCartByID(Long cartId);

    PromotionModel addPromotion(Long cartId, PromotionModel promotion);

    ItemModel addItem(Long cartId, ProductModel product, int qty);

    ItemModel updateCartItem(long itemId, int qty);

    Long deleteCart(Long cartId);

    List<ItemModel> getCartItems(Long cartId);

    Long deleteCartItem(Long cartId, Long itemId);
}
