package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.dtos.PriceDto;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.services.adapters.PriceServiceAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class DefaultPriceService implements PriceService {
    private final PriceServiceAdapter priceServiceAdapter;

    @Autowired
    public DefaultPriceService(PriceServiceAdapter priceServiceAdapter) {
        this.priceServiceAdapter = priceServiceAdapter;
    }

    @Override
    public Double getPriceFor(ProductModel product) {
        Double price;
        try {
            price = getPriceForProduct(product);
        } catch (IOException e) {
            // TODO change exception
            throw new RuntimeException();
        }
        return price;
    }

    private Double getPriceForProduct(ProductModel product) throws IOException {
        return Optional.ofNullable(requestPriceByProductId(product))
                // TODO change exception
                .orElseThrow(RuntimeException::new)
                .getPrice();
    }

    private PriceDto requestPriceByProductId(ProductModel product) throws IOException {
        return priceServiceAdapter.getPriceByProductId(product.getId()).execute().body();
    }
}
