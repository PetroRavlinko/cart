package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.models.CartModel;
import com.ravlinko.ecommerce.cart.models.ItemModel;
import com.ravlinko.ecommerce.cart.models.ProductModel;
import com.ravlinko.ecommerce.cart.models.PromotionModel;
import com.ravlinko.ecommerce.cart.repositories.CartItemRepository;
import com.ravlinko.ecommerce.cart.repositories.CartRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log
@Service
public class DefaultCartService implements CartService {
    private final PriceService priceService;
    private final PromotionService promotionService;
    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;

    @Autowired
    public DefaultCartService(PriceService priceService, PromotionService promotionService, CartRepository cartRepository, CartItemRepository cartItemRepository) {
        this.priceService = priceService;
        this.promotionService = promotionService;
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public CartModel getCartByID(Long cartId) {
        return cartRepository.findById(cartId).orElseGet(() -> {
            CartModel cartModel = new CartModel();
            cartModel.setId(cartId);
            cartModel.setTotalPrice(.0);
            cartModel.setItems(new ArrayList<>());
            return cartRepository.save(cartModel);
        });
    }

    @Override
    public PromotionModel addPromotion(Long cartId, PromotionModel promotion) {
        CartModel cart = getCartByID(cartId);
        cart.setPromotion(promotion);
        refreshCart(cart);
        return promotion;
    }

    @Override
    public ItemModel addItem(Long cartId, ProductModel product, int qty) {
        CartModel cart = getCartByID(cartId);
        ItemModel cartItem = cart.getItems().stream()
                .filter(item -> item.getProduct().equals(product))
                .findFirst().orElseGet(() -> {
                    ItemModel item = new ItemModel();
                    item.setPrice(priceService.getPriceFor(product));
                    item.setProduct(product);
                    return addItem(cart, item);
                });

        cartItem.setQty(cartItem.getQty() + qty);
        ItemModel addedItem = cartItemRepository.save(cartItem);
        refreshCart(cart);
        return addedItem;
    }

    private ItemModel addItem(CartModel cart, ItemModel item) {
        item.setCart(cart);
        cart.getItems().add(item);
        return item;
    }

    private void refreshCart(CartModel cart) {
        double totalPrice = cart.getItems().stream()
                .collect(Collectors.summarizingDouble(item -> item.getPrice() * item.getQty()))
                .getSum();
        if (Objects.nonNull(cart.getPromotion())) {
            try {
                totalPrice = totalPrice * (1 - promotionService.getPromotionDiscount(cart.getPromotion())/100) ;
            }catch (RuntimeException e) {
                cart.setPromotion(null);
            }
        }
        DecimalFormat df = new DecimalFormat("#.##");
        cart.setTotalPrice(Double.valueOf(df.format(totalPrice)));
        cartRepository.save(cart);
    }

    private void updateItem(ItemModel cartItem, int qty) {
        cartItem.setId(cartItem.getId());
        cartItem.setQty(qty);
        cartItemRepository.save(cartItem);
    }

    @Override
    public ItemModel updateCartItem(long itemId, int qty) {
        ItemModel item = cartItemRepository.findById(itemId).orElseThrow(RuntimeException::new);
        updateItem(item, qty);
        refreshCart(item.getCart());
        return item;
    }

    @Override
    public Long deleteCart(Long cartId) {
        cartRepository.deleteById(cartId);
        return cartId;
    }

    @Override
    public List<ItemModel> getCartItems(Long cartId) {
        return getCartByID(cartId).getItems();
    }

    @Override
    public Long deleteCartItem(Long cartId, Long itemId) {
        cartRepository.findById(cartId).ifPresent(
                cart ->
                        cart.getItems()
                                .stream()
                                .filter(item -> item.getId().equals(itemId))
                                .findFirst()
                                .ifPresent(item -> {
                                    cart.getItems().remove(item);
                                    refreshCart(cart);
                                    cartItemRepository.delete(item);
                                }));
        return itemId;
    }
}
