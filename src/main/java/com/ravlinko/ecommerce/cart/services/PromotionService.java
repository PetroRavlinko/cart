package com.ravlinko.ecommerce.cart.services;

import com.ravlinko.ecommerce.cart.models.PromotionModel;

public interface PromotionService {
    double getPromotionDiscount(PromotionModel promotion);
}
