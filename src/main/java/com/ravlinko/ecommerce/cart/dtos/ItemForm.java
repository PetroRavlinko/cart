package com.ravlinko.ecommerce.cart.dtos;

import lombok.Data;

@Data
public class ItemForm {
    /**
     * Product id
     */
    private long id;

    /**
     * Quantity
     */
    private int qty;
}
