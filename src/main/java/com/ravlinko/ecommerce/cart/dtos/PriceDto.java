package com.ravlinko.ecommerce.cart.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PriceDto {
    private Double price;
}
