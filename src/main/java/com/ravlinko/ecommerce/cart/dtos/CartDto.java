package com.ravlinko.ecommerce.cart.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
public class CartDto {
    private Long id;
    private Double totalPrice;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private PromotionDto promotion;
    private List<ItemDto> items;
}
