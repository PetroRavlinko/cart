package com.ravlinko.ecommerce.cart.dtos;

import lombok.Data;

@Data
public class PromotionDto {
    private Long id;
    private String code;
}
