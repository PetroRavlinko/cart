package com.ravlinko.ecommerce.cart.dtos;

import lombok.Data;

@Data
public class ItemDto {
    private Long id;
    private Integer qty;
    private Double price;
    private ProductDto product;
}
