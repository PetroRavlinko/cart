package com.ravlinko.ecommerce.cart.dtos;

import lombok.Data;

@Data
public class PromotionForm {
    private Long id;
    private String code;
}
