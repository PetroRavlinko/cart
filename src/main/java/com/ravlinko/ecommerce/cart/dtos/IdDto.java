package com.ravlinko.ecommerce.cart.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IdDto {
    private Long id;
}
